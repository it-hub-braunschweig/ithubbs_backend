package de.stevenschwenke.java.ithubbs.ithubbsbackend.conf;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.jdbc.config.annotation.EnableRdsInstance;
import org.springframework.cloud.aws.jdbc.config.annotation.RdsInstanceConfigurer;
import org.springframework.cloud.aws.jdbc.datasource.DataSourceInformation;
import org.springframework.cloud.aws.jdbc.datasource.TomcatJdbcDataSourceFactory;
import org.springframework.cloud.aws.jdbc.datasource.support.DatabaseType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile({"test", "prod"})
public class DatabaseConfigTestAndProd {

    @Value("${rds_host}")
    private String rdsHost;

    @Value("${rds_port}")
    private String rdsPort;

    @Value("${rds_database_name}")
    private String rdsDatabaseName;

    @Value("${rds_database_user}")
    private String rdsDatabaseUser;

    @Value("${rds_secret}")
    private String rdsSecret;

    @Bean
    public RdsInstanceConfigurer instanceConfigurer() {
        return () -> {
            TomcatJdbcDataSourceFactory dataSourceFactory
                    = new TomcatJdbcDataSourceFactory();
            dataSourceFactory.setInitialSize(10);
            dataSourceFactory.setValidationQuery("SELECT 1");
            return dataSourceFactory;
        };
    }

    @Bean
    SpringLiquibase liquibase() {

        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(instanceConfigurer().getDataSourceFactory().createDataSource(new DataSourceInformation(
                DatabaseType.POSTGRES,
                rdsHost,
                Integer.valueOf(rdsPort),
                rdsDatabaseName,
                rdsDatabaseUser,
                rdsSecret)));
        liquibase.setChangeLog("classpath:/liquibase/liquibase-changelog.xml");
        return liquibase;
    }
}
